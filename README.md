## Requirements

node@18.13.0
npm@9.4.0


## Getting Started

Install all the dependencies

```bash
npm i
```

Run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

