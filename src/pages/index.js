import Head from 'next/head'
import Image from 'next/image'
import BeerList from "../components/BeerList"
import { useEffect, useState } from 'react'

const defaultUrl = "https://api.punkapi.com/v2/beers?per_page=15"

export const getStaticProps = async () => {
  const res = await fetch(defaultUrl);
  const data = await res.json();

  const bigList = await fetch("https://api.punkapi.com/v2/beers?per_page=80")
  const bigListData = await bigList.json()

  return {
    props: {initialProps: data, longList: bigListData},
  }
}
 
export default function Home({initialProps, longList}) {
  const [beers, setBeers] = useState(initialProps)
  const [url, setUrl] = useState(defaultUrl)
  const [page, setPage] = useState(1)
  const pagination = [1,2,3,4,5,6,7,8,9,10];
  const hopNames = []
  const hopsList = []
  const maltsList = []
  const maltNames = []
  const yeastNames = []
  const foodPairingsList = []

  longList.map((beer)=>{
    hopsList.push(beer.ingredients.hops)
    hopsList.map((hop)=>{
      hop.map((hopName)=>{
      if(!hopNames.includes(hopName.name))
        {
          hopNames.push(hopName.name)
        }
      })
    })
    maltsList.push(beer.ingredients.malt)
    maltsList.map((malt)=>{
      malt.map((maltName)=>{
        if(!maltNames.includes(maltName.name))
          {
            maltNames.push(maltName.name)
          }
      })
    })
    if(!yeastNames.includes(beer.ingredients.yeast)){
      yeastNames.push(beer.ingredients.yeast)
    }
    beer.food_pairing.map((fPairing)=>{
      if(!foodPairingsList.includes(fPairing))
      {
        foodPairingsList.push(fPairing)
      }
    })
  })
 
  const ingredients = {
    malts: maltNames,
    hops: hopNames,
    yeasts: yeastNames
  }

  const getUrl = (type, param) => {
    switch (type) {
      case "page":
        setUrl(`https://api.punkapi.com/v2/beers?page=${param}&per_page=15`)
        setPage(param)
        break;
      case "search":
        setUrl(`https://api.punkapi.com/v2/beers?beer_name=${param}`)
        setPage(1)
        break;
      case "malt":
        setUrl(`https://api.punkapi.com/v2/beers?malt=${param}`)
        setPage(1)
        break
      case "hops":
        setUrl(`https://api.punkapi.com/v2/beers?hops=${param}`)
        setPage(1)
        break
      case "yeast":
        setUrl(`https://api.punkapi.com/v2/beers?yeast=${param}`)
        setPage(1)
        break
      case "food":
        setUrl(`https://api.punkapi.com/v2/beers?food=${param}`)  
        setPage(1)
        break
      case "reset":
      default:
        setUrl(defaultUrl)
        setPage(1)
      break;
    }
  }

  useEffect(() => {
    async function requestBeers() {
      const res = await fetch(url)
      const newList = await res.json();
      setBeers(newList)
      console.log(newList)
    }
    requestBeers();
  }, [url]);
  
  return (
    <div className='m-[auto] w-8/12 max-w-4xl bg '>
      {/* TITLE */}
      <div className='text-center w-full my-4'>
        <h1 className='text-6xl font-semibold'>Beers</h1>
      </div>

      <div className='flex flex-wrap justify-between mb-4 space-y-4'>
        {/* SEARCH */}
        <input type="text" className='pl-2 mt-4 bg-neutral-800' placeholder='Search'
          onChange={(e)=>{
            e.preventDefault()
            const searchEntry = e.target.value.replaceAll(" ", "_") 
            if (searchEntry !=="") {
              getUrl("search", searchEntry)
            } else {
              getUrl("reset")
            }
          }}
        />
        {/* FILTERS */}
        <select className='bg-neutral-800'
          onChange={(e)=>{
          e.preventDefault()
            e.target.value === "reset" ?
            getUrl(e.target.value ) :
            getUrl("malt", e.target.value )
          }}
        >
          <option value={"reset"}> Filter by malt </option>
          {ingredients.malts.map((malt, i)=>
            <option key={i}
            value={malt.replaceAll(" ", "_")}
            >{malt}</option>
          )} 
        </select>
        <select className='bg-neutral-800'
          onChange={(e)=>{
          e.preventDefault()
            e.target.value === "reset" ?
            getUrl(e.target.value ) :
            getUrl("hops", e.target.value )
          }}
        >
          <option value={"reset"}> Filter by hop </option>
          {ingredients.hops.map((hop, i)=>
            <option key={i}
            value={hop.replaceAll(" ", "_")}
            >{hop}</option>
          )} 
        </select>
        <select className='bg-neutral-800'
          onChange={(e)=>{
            e.preventDefault()
            e.target.value === "reset" ?
            getUrl(e.target.value ) :
            getUrl("yeast", e.target.value )
          }}
        >
          <option value={"reset"}> Filter by yeast </option>
          {ingredients.yeasts.map((yeast, i)=>
            <option key={i}
            value={yeast.replaceAll(" ", "_")}
            >{yeast}</option>
          )} 
        </select>
        <select className='bg-neutral-800'
          onChange={(e)=>{
            e.preventDefault()
            e.target.value === "reset" ?
            getUrl(e.target.value ) :
            getUrl("food", e.target.value )
          }}
        >
          <option value={"reset"}> Filter by food pairing </option>
          {foodPairingsList.map((food, i)=>
            <option key={i}
            value={food.replaceAll(" ", "_")}
            >{food}</option>
          )} 
        </select>
      </div>
      {/* PAGINATION */}
      <div className='flex m-[auto] w-fit'>
        <div
          className='flex w-10 h-10 hover:bg-sky-800 hover:cursor-pointer items-center justify-center'
          onClick={()=> {
            getUrl( "page", page === 1? 10 : page - 1 )
          }} 
        >
          <p>←</p>
        </div>
        {pagination.map(i=>
          <div 
            key={i}
            className={`flex w-10 h-10 hover:bg-sky-800 hover:cursor-pointer items-center justify-center ${page === i ? "bg-neutral-800" : "" }`}
            onClick={()=> {
              getUrl("page", i)
            }}
          >
            <p>{i}</p>
          </div>
        )}
        <div 
          className='flex w-10 h-10 hover:bg-sky-800 hover:cursor-pointer items-center justify-center'
          onClick={()=> {
            getUrl("page", page === 10? 1 : page + 1 )
          }}
        >
          <p>→</p>
        </div>
      </div>
      <BeerList beers={beers}/>
    </div>
  )
}