import { useState } from "react";
import Modal from "./Modal";


const BeerList = ({beers}) => {
  const [ visualizedBeer , setVisualizedBeer] = useState(null)
  return (
    <>
      <table className={`table-fixed w-full ${visualizedBeer !== null? "blur-sm" : ""}`}>
        <thead>
          <tr className="text-left">
            <th className="text-2xl w-11 px-2">#</th>
            <th className="text-2xl">Name</th>
            <th className="text-2xl">Description</th>
            <th className="text-2xl w-16">Alc</th>
          </tr>
        </thead>
        <tbody>
          {
            beers.map(beer=>
                <tr key={beer.id} className='text-left hover:bg-sky-800 hover:cursor-pointer' onClick={()=> {setVisualizedBeer(beer)}}>
                  <th className="p-2 px-2">{beer.id !== null? beer.id: "?"}</th>
                  <th className="p-2 pl-0">{beer.name !== null? beer.name: "?"}</th>
                  <th className="p-2 pl-0">{beer.tagline !== null? beer.tagline: "?"}</th>
                  <th className="p-2 pl-0">{beer.abv !== null? beer.abv: "?"}%</th>
                </tr>
              )
            }
        </tbody>
      </table>
      {visualizedBeer!==null &&
       <div className="w-full h-full bg-black/50 fixed top-0 left-0 flex justify-center items-center flex-col" onClick={()=>{setVisualizedBeer(null)}}>
         <Modal beer={visualizedBeer}/>
          <p className="mt-2">Click anywhere to close the popup</p>   
        </div>
      }
    </>
  );
}

export default BeerList;