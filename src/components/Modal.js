const Modal = ({beer})=> {
  return (
    <div className="w-3/5 rounded-xl bg-sky-800 p-4 max-w-lg border-4 border-white max-h-[80%] overflow-auto" >
      <div className="text-center full-width ">
        <h3 className="text-xl">{beer.name}</h3>
      </div>
      <div className="w-full bg-white my-2 py-2">
        <img src={beer.image_url} className="h-[250px] mx-[auto]" alt="Beer image" />
      </div>
      <p>{beer.description}</p>
    </div>
  )    
}

export default Modal;
